
import { Component, OnInit } from '@angular/core';
import { CrudService } from '../crud.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})

export class CreatePage implements OnInit {

  fiVal: string = "";
  hiVal: string = "";
  hfVal: string = "";

  constructor(
   private crud: CrudService,
   private storage: Storage
  ) {
    this.crud.databaseConn(); 
  }

  ngOnInit() { }

  ionViewDidEnter() {  
    this.crud.getAllUsers()
  }
   
  createUser(){
    this.crud.addItem(this.fiVal, this.hiVal, this.hfVal);
  }
   /*
  remove(user) {
    this.crud.deleteUser(user);
  }*/
  
}