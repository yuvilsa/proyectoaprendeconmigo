import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SofaPage } from './sofa.page';

const routes: Routes = [
  {
    path: '',
    component: SofaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SofaPageRoutingModule {}
