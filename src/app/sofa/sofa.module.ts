import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SofaPageRoutingModule } from './sofa-routing.module';

import { SofaPage } from './sofa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SofaPageRoutingModule
  ],
  declarations: [SofaPage]
})
export class SofaPageModule {}
