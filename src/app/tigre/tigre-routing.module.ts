import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TigrePage } from './tigre.page';

const routes: Routes = [
  {
    path: '',
    component: TigrePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TigrePageRoutingModule {}
