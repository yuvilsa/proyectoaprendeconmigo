import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-tigre',
  templateUrl: './tigre.page.html',
  styleUrls: ['./tigre.page.scss'],
})
export class TigrePage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/gato']);
  }
  Continuar(){
    this.router.navigate(['/conejo']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/ti.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/gre.m4a');
    audio.load();
    audio.play();
  }
}
