import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TigrePageRoutingModule } from './tigre-routing.module';

import { TigrePage } from './tigre.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TigrePageRoutingModule
  ],
  declarations: [TigrePage]
})
export class TigrePageModule {}
