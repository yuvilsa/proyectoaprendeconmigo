import { Component, OnInit, ViewChild, HostListener, ElementRef, AfterViewInit } from '@angular/core';
import SignaturePad from 'signature_pad';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

declare var google;
@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
})

export class MapaPage implements OnInit {
  @ViewChild('canvas', { static: true }) signaturePadElement;
  signaturePad: any;
  canvasWidth: number;
  canvasHeight: number;
  constructor(private elementRef: ElementRef,
    private base64ToGallery: Base64ToGallery) { }


  
  ngOnInit(): void {
    this.init();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.init();
  }

  init() {
    const canvas: any = this.elementRef.nativeElement.querySelector('canvas');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight - 400;//140 --400
    if (this.signaturePad) {
      this.signaturePad.clear(); // Clear the pad on init
    }
  }
  ngAfterViewInit() {
    this.signaturePad = new SignaturePad(this.signaturePadElement.nativeElement);
    this.signaturePad.clear();
    this.signaturePad.penColor = 'red';//'rgb(56,128,255)' coloor
    this.signaturePad.line="50px";
    }

    save(): void {
      const img = this.signaturePad.toDataURL();
      this.base64ToGallery.base64ToGallery(img).then(
        res => console.log('Saved image to gallery ', res),
        err => console.log('Error saving image to gallery ', err)
      );
    }
  
    isCanvasBlank(): boolean {
      if (this.signaturePad) {
        return this.signaturePad.isEmpty() ? true : false;
      }
    }
  
    clear() {
      this.signaturePad.clear();
    }
  
    undo() {
      const data = this.signaturePad.toData();
      if (data) {
        data.pop(); // remove the last dot or line
        this.signaturePad.fromData(data);
      }
    }
}
