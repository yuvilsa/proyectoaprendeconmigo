import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-manzana',
  templateUrl: './manzana.page.html',
  styleUrls: ['./manzana.page.scss'],
})
export class ManzanaPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/banano']);
  }
  Continuar(){
    this.router.navigate(['/deletrear']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/man.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/zaa.m4a');
    audio.load();
    audio.play();
  }
  Animales3(){
    let audio = new Audio('./assets/audio/naa.m4a');
    audio.load();
    audio.play();
  }
}
