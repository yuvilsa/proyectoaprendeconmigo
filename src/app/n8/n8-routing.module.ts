import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { N8Page } from './n8.page';

const routes: Routes = [
  {
    path: '',
    component: N8Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class N8PageRoutingModule {}
