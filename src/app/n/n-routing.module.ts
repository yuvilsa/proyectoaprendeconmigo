import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NPage } from './n.page';

const routes: Routes = [
  {
    path: '',
    component: NPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NPageRoutingModule {}
