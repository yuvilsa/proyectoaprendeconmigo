import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';





const routes: Routes = [
  {
   // path: 'home',
    //loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
    path: 'pantalla-inicio',
    loadChildren: () => import('./pantalla-inicio/pantalla-inicio.module').then( m => m.PantallaInicioPageModule)
  },
  {
    path: '',
   // redirectTo: 'home',
   redirectTo: 'pantalla-inicio',
    pathMatch: 'full'
  },
/*
  {
    path: 'pantalla-inicio',
    loadChildren: () => import('./pantalla-inicio/pantalla-inicio.module').then( m => m.PantallaInicioPageModule)
  },
  */
  {
    path: 'menu-principal',
    loadChildren: () => import('./menu-principal/menu-principal.module').then( m => m.MenuPrincipalPageModule)
  },
  {
    path: 'sub-menu-aprendizaje',
    loadChildren: () => import('./sub-menu-aprendizaje/sub-menu-aprendizaje.module').then( m => m.SubMenuAprendizajePageModule)
  },
  {
    path: 'las-vocales',
    loadChildren: () => import('./las-vocales/las-vocales.module').then( m => m.LasVocalesPageModule)
  },/*
  {
    path: 'el-abecedario',
    loadChildren: () => import('./el-abecedario/el-abecedario.module').then( m => m.ElAbecedarioPageModule)
  },*/
  {
    path: 'los-numeros',
    loadChildren: () => import('./los-numeros/los-numeros.module').then( m => m.LosNumerosPageModule)
  },
  {
    path: 'completar-palabras',
    loadChildren: () => import('./completar-palabras/completar-palabras.module').then( m => m.CompletarPalabrasPageModule)
  },
  {
    path: 'configuraciones',
    loadChildren: () => import('./configuraciones/configuraciones.module').then( m => m.ConfiguracionesPageModule)
  },
  {
    path: 'ayuda',
    loadChildren: () => import('./ayuda/ayuda.module').then( m => m.AyudaPageModule)
  },
  {
    path: 'historial-ingreso',
    loadChildren: () => import('./historial-ingreso/historial-ingreso.module').then( m => m.HistorialIngresoPageModule)
  },
  {
    path: 'comprobacion-conocimientos',
    loadChildren: () => import('./comprobacion-conocimientos/comprobacion-conocimientos.module').then( m => m.ComprobacionConocimientosPageModule)
  },
  {
    path: 'probando',
    loadChildren: () => import('./probando/probando.module').then( m => m.ProbandoPageModule)
  },
  {
    path: 'pantalla-inicio',
    loadChildren: () => import('./pantalla-inicio/pantalla-inicio.module').then( m => m.PantallaInicioPageModule)
  },
  {
    path: 'test',
    loadChildren: () => import('./test/test.module').then( m => m.TestPageModule)
  },
  {
    path: 'sol',
    loadChildren: () => import('./sol/sol.module').then( m => m.SolPageModule)
  },
  {
    path: 'mapa',
    loadChildren: () => import('./mapa/mapa.module').then( m => m.MapaPageModule)
  },
  {
    path: 'vocales',
    loadChildren: () => import('./vocales/vocales.module').then( m => m.VocalesPageModule)
  },
  {
    path: 'abecedario',
    loadChildren: () => import('./Abecedario/abecedario.module').then( m => m.AbecedarioPageModule)
  },
  {
    path: 'a',
    loadChildren: () => import('./A/a.module').then( m => m.APageModule)
  },
  {
    path: 'b',
    loadChildren: () => import('./B/b.module').then( m => m.BPageModule)
  },
  {
    path: 'c',
    loadChildren: () => import('./C/c.module').then( m => m.CPageModule)
  },
  {
    path: 'd',
    loadChildren: () => import('./D/d.module').then( m => m.DPageModule)
  },
  {
    path: 'e',
    loadChildren: () => import('./E/e.module').then( m => m.EPageModule)
  },
  {
    path: 'f',
    loadChildren: () => import('./F/f.module').then( m => m.FPageModule)
  },
  {
    path: 'g',
    loadChildren: () => import('./G/g.module').then( m => m.GPageModule)
  },
  {
    path: 'h',
    loadChildren: () => import('./H/h.module').then( m => m.HPageModule)
  },
  {
    path: 'i',
    loadChildren: () => import('./I/i.module').then( m => m.IPageModule)
  },
  {
    path: 'j',
    loadChildren: () => import('./J/j.module').then( m => m.JPageModule)
  },
  {
    path: 'k',
    loadChildren: () => import('./K/k.module').then( m => m.KPageModule)
  },
  {
    path: 'l',
    loadChildren: () => import('./L/l.module').then( m => m.LPageModule)
  },
  {
    path: 'm',
    loadChildren: () => import('./M/m.module').then( m => m.MPageModule)
  },
  {
    path: 'n',
    loadChildren: () => import('./N/n.module').then( m => m.NPageModule)
  },
  {
    path: 'o',
    loadChildren: () => import('./O/o.module').then( m => m.OPageModule)
  },
  {
    path: 'p',
    loadChildren: () => import('./P/p.module').then( m => m.PPageModule)
  },
  {
    path: 'q',
    loadChildren: () => import('./Q/q.module').then( m => m.QPageModule)
  },
  {
    path: 'r',
    loadChildren: () => import('./R/r.module').then( m => m.RPageModule)
  },
  {
    path: 's',
    loadChildren: () => import('./S/s.module').then( m => m.SPageModule)
  },
  {
    path: 't',
    loadChildren: () => import('./T/t.module').then( m => m.TPageModule)
  },
  {
    path: 'u',
    loadChildren: () => import('./U/u.module').then( m => m.UPageModule)
  },
  {
    path: 'v',
    loadChildren: () => import('./V/v.module').then( m => m.VPageModule)
  },
  {
    path: 'w',
    loadChildren: () => import('./W/w.module').then( m => m.WPageModule)
  },
  {
    path: 'x',
    loadChildren: () => import('./X/x.module').then( m => m.XPageModule)
  },
  {
    path: 'y',
    loadChildren: () => import('./Y/y.module').then( m => m.YPageModule)
  },
  {
    path: 'z',
    loadChildren: () => import('./Z/z.module').then( m => m.ZPageModule)
  },
  {
    path: 'aa',
    loadChildren: () => import('./Aa/aa.module').then( m => m.AaPageModule)
  },  
  {
    path: 'ee',
    loadChildren: () => import('./Ee/ee.module').then( m => m.EePageModule)
  },  
  {
    path: 'ii',
    loadChildren: () => import('./Ii/ii.module').then( m => m.IiPageModule)
  },  
  {
    path: 'oo',
    loadChildren: () => import('./Oo/oo.module').then( m => m.OoPageModule)
  },  
  {
    path: 'uu',
    loadChildren: () => import('./Uu/uu.module').then( m => m.UuPageModule)
  },

  {
    path: 'n0',
    loadChildren: () => import('./N0/n0.module').then( m => m.N0PageModule)
  },
  {
    path: 'n1',
    loadChildren: () => import('./N1/n1.module').then( m => m.N1PageModule)
  },
  {
    path: 'n2',
    loadChildren: () => import('./N2/n2.module').then( m => m.N2PageModule)
  },
  {
    path: 'n3',
    loadChildren: () => import('./N3/n3.module').then( m => m.N3PageModule)
  },
  {
    path: 'n4',
    loadChildren: () => import('./N4/n4.module').then( m => m.N4PageModule)
  },
  {
    path: 'n5',
    loadChildren: () => import('./N5/n5.module').then( m => m.N5PageModule)
  },
  {
    path: 'n6',
    loadChildren: () => import('./N6/n6.module').then( m => m.N6PageModule)
  },
  {
    path: 'n7',
    loadChildren: () => import('./N7/n7.module').then( m => m.N7PageModule)
  },
  {
    path: 'n8',
    loadChildren: () => import('./N8/n8.module').then( m => m.N8PageModule)
  },
  {
    path: 'n9',
    loadChildren: () => import('./N9/n9.module').then( m => m.N9PageModule)
  },
  {
    path: 'ba',
    loadChildren: () => import('./Ba/ba.module').then( m => m.BaPageModule)
  },
  {
    path: 'ca',
    loadChildren: () => import('./ca/ca.module').then( m => m.CaPageModule)
  },
  {
    path: 'da',
    loadChildren: () => import('./Da/da.module').then( m => m.DaPageModule)
  },
  {
    path: 'fa',
    loadChildren: () => import('./Fa/fa.module').then( m => m.FaPageModule)
  },
  {
    path: 'ga',
    loadChildren: () => import('./Ga/ga.module').then( m => m.GaPageModule)
  },
  {
    path: 'ja',
    loadChildren: () => import('./Ja/ja.module').then( m => m.JaPageModule)
  },
  {
    path: 'ka',
    loadChildren: () => import('./Ka/ka.module').then( m => m.KaPageModule)
  },
  {
    path: 'la',
    loadChildren: () => import('./La/la.module').then( m => m.LaPageModule)
  },
  {
    path: 'ma',
    loadChildren: () => import('./Ma/ma.module').then( m => m.MaPageModule)
  },
  {
    path: 'na',
    loadChildren: () => import('./Na/na.module').then( m => m.NaPageModule)
  },
  {
    path: 'pa',
    loadChildren: () => import('./Pa/pa.module').then( m => m.PaPageModule)
  },
  {
    path: 'qa',
    loadChildren: () => import('./Qa/qa.module').then( m => m.QaPageModule)
  },
  {
    path: 'ra',
    loadChildren: () => import('./Ra/ra.module').then( m => m.RaPageModule)
  },
  {
    path: 'sa',
    loadChildren: () => import('./Sa/sa.module').then( m => m.SaPageModule)
  },
  {
    path: 'ta',
    loadChildren: () => import('./Ta/ta.module').then( m => m.TaPageModule)
  },
  {
    path: 'va',
    loadChildren: () => import('./Va/va.module').then( m => m.VaPageModule)
  },
  {
    path: 'wa',
    loadChildren: () => import('./Wa/wa.module').then( m => m.WaPageModule)
  },
  {
    path: 'xa',
    loadChildren: () => import('./Xa/xa.module').then( m => m.XaPageModule)
  },
  {
    path: 'ya',
    loadChildren: () => import('./Ya/ya.module').then( m => m.YaPageModule)
  },
  {
    path: 'za',
    loadChildren: () => import('./Za/za.module').then( m => m.ZaPageModule)
  },
  {
    path: 'deletrear',
    loadChildren: () => import('./deletrear/deletrear.module').then( m => m.DeletrearPageModule)
  },
  {
    path: 'perro',
    loadChildren: () => import('./Perro/perro.module').then( m => m.PerroPageModule)
  },
  {
    path: 'ballena',
    loadChildren: () => import('./Ballena/ballena.module').then( m => m.BallenaPageModule)
  },
  {
    path: 'burro',
    loadChildren: () => import('./Burro/burro.module').then( m => m.BurroPageModule)
  },
  {
    path: 'gato',
    loadChildren: () => import('./Gato/gato.module').then( m => m.GatoPageModule)
  },
  {
    path: 'tigre',
    loadChildren: () => import('./Tigre/tigre.module').then( m => m.TigrePageModule)
  },
  {
    path: 'conejo',
    loadChildren: () => import('./Conejo/conejo.module').then( m => m.ConejoPageModule)
  },
  {
    path: 'caballo',
    loadChildren: () => import('./Caballo/caballo.module').then( m => m.CaballoPageModule)
  },
  {
    path: 'banano',
    loadChildren: () => import('./Banano/banano.module').then( m => m.BananoPageModule)
  },
  {
    path: 'manzana',
    loadChildren: () => import('./Manzana/manzana.module').then( m => m.ManzanaPageModule)
  },
  //HOGAR
  {
    path: 'cama',
    loadChildren: () => import('./Cama/cama.module').then( m => m.CamaPageModule)
  },
  {
    path: 'silla',
    loadChildren: () => import('./Silla/silla.module').then( m => m.SillaPageModule)
  },
  {
    path: 'sofa',
    loadChildren: () => import('./Sofa/sofa.module').then( m => m.SofaPageModule)
  },
  {
    path: 'mesa',
    loadChildren: () => import('./Mesa/mesa.module').then( m => m.MesaPageModule)
  },
  {
    path: 'cortina',
    loadChildren: () => import('./Cortina/cortina.module').then( m => m.CortinaPageModule)
  },
  {
    path: 'martillo',
    loadChildren: () => import('./Martillo/martillo.module').then( m => m.MartilloPageModule)
  },
  {
    path: 'serrucho',
    loadChildren: () => import('./Serrucho/serrucho.module').then( m => m.SerruchoPageModule)
  },
  {
    path: 'prueba1',
    loadChildren: () => import('./prueba1/prueba1.module').then( m => m.Prueba1PageModule)
  },
  {
    path: 'prueba2',
    loadChildren: () => import('./prueba2/prueba2.module').then( m => m.Prueba2PageModule)
  },
  {
    path: 'agregar',
    loadChildren: () => import('./agregar/agregar.module').then( m => m.AgregarPageModule)
  },
  {
    path: 'editar',
    loadChildren: () => import('./editar/editar.module').then( m => m.EditarPageModule)
  },
  {
    path: 'edi',
    loadChildren: () => import('./edi/edi.module').then( m => m.EdiPageModule)
  },
  /*{
    path: 'edit/:id',
    loadChildren: () => import('./edit/edit.module').then( m => m.EditPageModule)
  },*/
  {
    path: 'create',
    loadChildren: () => import('./create/create.module').then( m => m.CreatePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

