import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BananoPage } from './banano.page';

const routes: Routes = [
  {
    path: '',
    component: BananoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BananoPageRoutingModule {}
