import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BananoPageRoutingModule } from './banano-routing.module';

import { BananoPage } from './banano.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BananoPageRoutingModule
  ],
  declarations: [BananoPage]
})
export class BananoPageModule {}
