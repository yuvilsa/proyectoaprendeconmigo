import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-banano',
  templateUrl: './banano.page.html',
  styleUrls: ['./banano.page.scss'],
})
export class BananoPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/deletrear']);
  }
  Continuar(){
    this.router.navigate(['/manzana']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/baa.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/naa.m4a');
    audio.load();
    audio.play();
  }
  Animales3(){
    let audio = new Audio('./assets/audio/noo.m4a');
    audio.load();
    audio.play();
  }
}
