import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { XPage } from './x.page';

const routes: Routes = [
  {
    path: '',
    component: XPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class XPageRoutingModule {}
