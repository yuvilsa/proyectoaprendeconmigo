import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PantallaInicioPage } from './pantalla-inicio.page';

const routes: Routes = [
  {
    path: '',
    component: PantallaInicioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PantallaInicioPageRoutingModule {}
