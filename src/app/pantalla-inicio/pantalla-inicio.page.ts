import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';




@Component({
  selector: 'app-pantalla-inicio',
  templateUrl: './pantalla-inicio.page.html',
  styleUrls: ['./pantalla-inicio.page.scss'],
})
export class PantallaInicioPage implements OnInit {
  currentDate: String = new Date().toLocaleDateString();
  //Fecha: String='Hola';
 // public Fecha1: String;
 //variables públicas
  public Fecha: String=new Date().toLocaleDateString();
  public Hora=new Date().toLocaleTimeString();
  constructor(
    private router: Router,
    private alertCtrl: AlertController,
    private storage: Storage
    ) { }
    

  ngOnInit() {
  }
  async Continuar(){
    let audio = new Audio('./assets/audio/b9.m4a');
    audio.load();
    audio.play();
    this.router.navigate(['/menu-principal']);
   console.log(this.Hora);
    //busqueda de usuario almacenado en la variable usuario
    const storage = await this.storage.create(); //para inicializar --leer--guardar
    //const us = await storage.get('usuario');
     await storage.set('FI',this.Fecha); //variable
     await storage.set('HI',this.Hora); //variable

}
Create(){
  this.router.navigate(['/create']);
}
Edit(){
  this.router.navigate(['/edit']);
}

}
