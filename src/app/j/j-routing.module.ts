import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JPage } from './j.page';

const routes: Routes = [
  {
    path: '',
    component: JPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JPageRoutingModule {}
