import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-mesa',
  templateUrl: './mesa.page.html',
  styleUrls: ['./mesa.page.scss'],
})
export class MesaPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/sofa']);
  }
  Continuar(){
    this.router.navigate(['/cortina']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/me.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/saa.m4a');
    audio.load();
    audio.play();
  }
}
