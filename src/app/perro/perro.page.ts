import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-perro',
  templateUrl: './perro.page.html',
  styleUrls: ['./perro.page.scss'],
})
export class PerroPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/deletrear']);
  }
  Continuar(){
    this.router.navigate(['/ballena']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/pee.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/rro.m4a');
    audio.load();
    audio.play();
  }
}
