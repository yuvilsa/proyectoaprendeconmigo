import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VaPage } from './va.page';

const routes: Routes = [
  {
    path: '',
    component: VaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VaPageRoutingModule {}
