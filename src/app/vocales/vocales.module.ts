import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { VocalesPageRoutingModule } from './vocales-routing.module';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { VocalesPage } from './vocales.page';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VocalesPageRoutingModule,
    
  ],
  providers: [
    AndroidPermissions,
    Base64ToGallery,

  ],
  declarations: [VocalesPage]
})
export class VocalesPageModule {}
