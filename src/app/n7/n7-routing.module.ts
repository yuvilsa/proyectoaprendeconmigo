import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { N7Page } from './n7.page';

const routes: Routes = [
  {
    path: '',
    component: N7Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class N7PageRoutingModule {}
