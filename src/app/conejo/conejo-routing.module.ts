import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConejoPage } from './conejo.page';

const routes: Routes = [
  {
    path: '',
    component: ConejoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConejoPageRoutingModule {}
