import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-conejo',
  templateUrl: './conejo.page.html',
  styleUrls: ['./conejo.page.scss'],
})
export class ConejoPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/tigre']);
  }
  Continuar(){
    this.router.navigate(['/caballo']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/co.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/ne.m4a');
    audio.load();
    audio.play();
  }
  Animales3(){
    let audio = new Audio('./assets/audio/jo.m4a');
    audio.load();
    audio.play();
  }
}
