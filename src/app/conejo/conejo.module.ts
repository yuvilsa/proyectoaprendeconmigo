import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConejoPageRoutingModule } from './conejo-routing.module';

import { ConejoPage } from './conejo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConejoPageRoutingModule
  ],
  declarations: [ConejoPage]
})
export class ConejoPageModule {}
