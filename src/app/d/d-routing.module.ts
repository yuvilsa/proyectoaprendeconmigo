import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DPage } from './d.page';

const routes: Routes = [
  {
    path: '',
    component: DPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DPageRoutingModule {}
