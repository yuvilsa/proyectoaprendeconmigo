import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TPage } from './t.page';

const routes: Routes = [
  {
    path: '',
    component: TPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TPageRoutingModule {}
