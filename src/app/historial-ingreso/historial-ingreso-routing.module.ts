import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistorialIngresoPage } from './historial-ingreso.page';

const routes: Routes = [
  {
    path: '',
    component: HistorialIngresoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistorialIngresoPageRoutingModule {}
