import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistorialIngresoPageRoutingModule } from './historial-ingreso-routing.module';

import { HistorialIngresoPage } from './historial-ingreso.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistorialIngresoPageRoutingModule
  ],
  declarations: [HistorialIngresoPage]
})
export class HistorialIngresoPageModule {}
