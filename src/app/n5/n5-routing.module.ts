import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { N5Page } from './n5.page';

const routes: Routes = [
  {
    path: '',
    component: N5Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class N5PageRoutingModule {}
