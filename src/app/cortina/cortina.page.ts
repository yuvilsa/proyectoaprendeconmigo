import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-cortina',
  templateUrl: './cortina.page.html',
  styleUrls: ['./cortina.page.scss'],
})
export class CortinaPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/mesa']);
  }
  Continuar(){
    this.router.navigate(['/deletrear']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/cor.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/ti.m4a');
    audio.load();
    audio.play();
  }
  Animales3(){
    let audio = new Audio('./assets/audio/naa.m4a');
    audio.load();
    audio.play();
  }
}
