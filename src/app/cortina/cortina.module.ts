import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CortinaPageRoutingModule } from './cortina-routing.module';

import { CortinaPage } from './cortina.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CortinaPageRoutingModule
  ],
  declarations: [CortinaPage]
})
export class CortinaPageModule {}
