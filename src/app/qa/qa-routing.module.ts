import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QaPage } from './qa.page';

const routes: Routes = [
  {
    path: '',
    component: QaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QaPageRoutingModule {}
