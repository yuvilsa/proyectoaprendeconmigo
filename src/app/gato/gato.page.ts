import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-gato',
  templateUrl: './gato.page.html',
  styleUrls: ['./gato.page.scss'],
})
export class GatoPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/burro']);
  }
  Continuar(){
    this.router.navigate(['/tigre']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/gaa.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/to.m4a');
    audio.load();
    audio.play();
  }
}
