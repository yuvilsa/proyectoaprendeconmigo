import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { NaPageRoutingModule } from './na-routing.module';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { NaPage } from './na.page';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NaPageRoutingModule,
    
  ],
  providers: [
    AndroidPermissions,
    Base64ToGallery,

  ],
  declarations: [NaPage]
})
export class NaPageModule {}
