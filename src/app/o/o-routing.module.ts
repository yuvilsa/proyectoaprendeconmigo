import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OPage } from './o.page';

const routes: Routes = [
  {
    path: '',
    component: OPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OPageRoutingModule {}
