import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-martillo',
  templateUrl: './martillo.page.html',
  styleUrls: ['./martillo.page.scss'],
})
export class MartilloPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/deletrear']);
  }
  Continuar(){
    this.router.navigate(['/serrucho']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/mar.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/ti.m4a');
    audio.load();
    audio.play();
  }
  Animales3(){
    let audio = new Audio('./assets/audio/Lloo.m4a');
    audio.load();
    audio.play();
  }
}
