import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MartilloPageRoutingModule } from './martillo-routing.module';

import { MartilloPage } from './martillo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MartilloPageRoutingModule
  ],
  declarations: [MartilloPage]
})
export class MartilloPageModule {}
