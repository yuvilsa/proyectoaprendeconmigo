import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MartilloPage } from './martillo.page';

const routes: Routes = [
  {
    path: '',
    component: MartilloPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MartilloPageRoutingModule {}
