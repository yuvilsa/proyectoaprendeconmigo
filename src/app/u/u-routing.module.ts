import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UPage } from './u.page';

const routes: Routes = [
  {
    path: '',
    component: UPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UPageRoutingModule {}
