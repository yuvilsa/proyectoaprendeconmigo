import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-configuraciones',
  templateUrl: './configuraciones.page.html',
  styleUrls: ['./configuraciones.page.scss'],
})
export class ConfiguracionesPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  Ayuda(){
    this.router.navigate(['/ayuda']);
  }
  Historialingreso(){
    this.router.navigate(['/historial-ingreso']);
  }
  Regresar(){
    this.router.navigate(['/menu-principal']); 
  }

}
