import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WPage } from './w.page';

const routes: Routes = [
  {
    path: '',
    component: WPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WPageRoutingModule {}
