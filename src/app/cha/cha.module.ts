import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ChaPageRoutingModule } from './cha-routing.module';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { ChaPage } from './cha.page';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChaPageRoutingModule,
    
  ],
  providers: [
    AndroidPermissions,
    Base64ToGallery,

  ],
  declarations: [ChaPage]
})
export class ChaPageModule {}
