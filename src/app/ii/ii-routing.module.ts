import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IiPage } from './ii.page';

const routes: Routes = [
  {
    path: '',
    component: IiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IiPageRoutingModule {}
