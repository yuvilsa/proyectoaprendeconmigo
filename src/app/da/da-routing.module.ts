import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaPage } from './da.page';

const routes: Routes = [
  {
    path: '',
    component: DaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaPageRoutingModule {}
