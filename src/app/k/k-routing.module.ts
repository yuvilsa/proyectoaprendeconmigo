import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KPage } from './k.page';

const routes: Routes = [
  {
    path: '',
    component: KPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KPageRoutingModule {}
