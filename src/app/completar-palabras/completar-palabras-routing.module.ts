import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompletarPalabrasPage } from './completar-palabras.page';

const routes: Routes = [
  {
    path: '',
    component: CompletarPalabrasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompletarPalabrasPageRoutingModule {}
