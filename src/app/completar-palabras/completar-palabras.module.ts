import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CompletarPalabrasPageRoutingModule } from './completar-palabras-routing.module';

import { CompletarPalabrasPage } from './completar-palabras.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CompletarPalabrasPageRoutingModule
  ],
  declarations: [CompletarPalabrasPage]
})
export class CompletarPalabrasPageModule {}
