import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-cama',
  templateUrl: './cama.page.html',
  styleUrls: ['./cama.page.scss'],
})
export class CamaPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }


  Regresar(){
    this.router.navigate(['/mesa']);
  }
  Continuar(){
    this.router.navigate(['/cortina']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/caa.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/maa.m4a');
    audio.load();
    audio.play();
  }

}
