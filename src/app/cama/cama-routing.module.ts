import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CamaPage } from './Cama.page';

const routes: Routes = [
  {
    path: '',
    component: CamaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CamaPageRoutingModule {}
