import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CamaPageRoutingModule } from './cama-routing.module';

import { CamaPage } from './cama.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CamaPageRoutingModule
  ],
  declarations: [CamaPage]
})
export class CamaPageModule {}
