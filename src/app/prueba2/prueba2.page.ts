import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import {Router} from '@angular/router';



@Component({
  selector: 'app-prueba2',
  templateUrl: './prueba2.page.html',
  styleUrls: ['./prueba2.page.scss'],
})
export class Prueba2Page implements OnInit {

  constructor( private alertCtrl: AlertController,
    public alertController: AlertController, private router: Router) { }

  ngOnInit() {
  }
  sonido(){
    let audio = new Audio('./assets/audio/sel2.m4a');
    audio.load();
    audio.play();
  }

  async Aprendizaje() {
    let audio = new Audio('./assets/audio/nook.m4a');
    audio.load();
    audio.play();
    const alert = await this.alertController.create({
      // header: 'Confirm!',
      message: 'RESPUESTA INCORRECTA!',
      buttons: [
        {
          text: 'Aceptar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.router.navigate(['/prueba2']);
            //let audio = new Audio('./assets/audio/b10.m4a');
           // audio.load();
           // audio.play();
          }
        }
      ]
    });

    await alert.present();
  }

  async Comprobacionconocimientos() {
    let audio = new Audio('./assets/audio/nook.m4a');
    audio.load();
    audio.play();
    const alert = await this.alertController.create({
      // header: 'Confirm!',
      message: 'RESPUESTA INCORRECTA!',
      buttons: [
        {
          text: 'Aceptar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.router.navigate(['/prueba2']);
            //let audio = new Audio('./assets/audio/b10.m4a');
           // audio.load();
           // audio.play();
          }
        }
      ]
    });

    await alert.present();
  }

  
  async Configuraciones() {
    let audio = new Audio('./assets/audio/nook.m4a');
    audio.load();
    audio.play();
    const alert = await this.alertController.create({
      // header: 'Confirm!',
      message: 'RESPUESTA INCORRECTA!',
      buttons: [
        {
          text: 'Aceptar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.router.navigate(['/prueba2']);
            //let audio = new Audio('./assets/audio/b10.m4a');
           // audio.load();
           // audio.play();
          }
        }
      ]
    });

    await alert.present();
  }


  
  async Salir() {
    let audio = new Audio('./assets/audio/ok.m4a');
    audio.load();
    audio.play();
    const alert = await this.alertController.create({
      // header: 'Confirm!',
      message: 'RESPUESTA CORRECTA!',
      buttons: [
        {
          text: 'Aceptar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.router.navigate(['/prueba2']);
            //let audio = new Audio('./assets/audio/b10.m4a');
           // audio.load();
           // audio.play();
          }
        }
      ]
    });

    await alert.present();
  }


}
