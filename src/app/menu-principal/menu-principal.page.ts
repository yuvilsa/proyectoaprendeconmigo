import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-menu-principal',
  templateUrl: './menu-principal.page.html',
  styleUrls: ['./menu-principal.page.scss'],
})

export class MenuPrincipalPage implements OnInit {
  bandera: String='';
  public FechaF: String=new Date().toLocaleDateString();
  public HoraF=new Date().toLocaleTimeString();

  constructor(private router:Router,
    private alertCtrl: AlertController,
    public alertController: AlertController,
    private storage: Storage,
    private crud: CrudService) { }

  ngOnInit() {
  }

  Aprendizaje(){
    let audio = new Audio('./assets/audio/b11.m4a');
    audio.load();
    audio.play();
    this.router.navigate(['/sub-menu-aprendizaje']);
   // console.log($Fecha1);
  }

  Configuraciones(){
    let audio = new Audio('./assets/audio/b12.m4a');
    audio.load();
    audio.play();
    this.router.navigate(['/configuraciones']);

  }
  Comprobacionconocimientos(){
    let audio = new Audio('./assets/audio/b13.m4a');
    audio.load();
    audio.play();
    this.router.navigate(['/comprobacion-conocimientos']);
  }
  
  /*
  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Low battery',
      subTitle: '10% of battery remaining',
      buttons: ['Dismiss']
    });
    alert.present();
  }*/
  si(){

  }
 
  async Salirt () {
    let audio = new Audio('./assets/audio/0.m4a');
    audio.load();
    audio.play();
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['Sí',  'No',]
    });

    await alert.present();
  }
  async Salir() {
    let audio = new Audio('./assets/audio/b10.m4a');
    audio.load();
    audio.play();
    const alert = await this.alertController.create({
      // header: 'Confirm!',
      message: 'Estás seguro de salir de la Aplicación?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //let audio = new Audio('./assets/audio/b10.m4a');
           // audio.load();
           // audio.play();
           this.bandera='0';
          }
        }, {
          text: 'Sí',
          handler: () => {
            //navigator['app'].exitApp();
           // ionic.Platform.exitApp();
           this.bandera='1';
           (navigator as any).app.exitApp();

          }
        }
      ]
    });

    await alert.present();
    if (this.bandera=='1') {
      const storage1 = await this.storage.create();
      await storage1.set('HF',this.HoraF);
      this.crud.addItem(await storage1.get('FI'), await storage1.get('HI'),await storage1.get('HF'));
    }else {

    }
  }

  
}
