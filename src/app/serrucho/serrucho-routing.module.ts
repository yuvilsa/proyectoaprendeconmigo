import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SerruchoPage } from './serrucho.page';

const routes: Routes = [
  {
    path: '',
    component: SerruchoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SerruchoPageRoutingModule {}
