import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-serrucho',
  templateUrl: './serrucho.page.html',
  styleUrls: ['./serrucho.page.scss'],
})
export class SerruchoPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/martillo']);
  }
  Continuar(){
    this.router.navigate(['/deletrear']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/see.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/rru.m4a');
    audio.load();
    audio.play();
  }
  Animales3(){
    let audio = new Audio('./assets/audio/cho.m4a');
    audio.load();
    audio.play();
  }
}
