import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SerruchoPageRoutingModule } from './serrucho-routing.module';

import { SerruchoPage } from './serrucho.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SerruchoPageRoutingModule
  ],
  declarations: [SerruchoPage]
})
export class SerruchoPageModule {}
