import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { PaPageRoutingModule } from './pa-routing.module';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { PaPage } from './pa.page';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaPageRoutingModule,
    
  ],
  providers: [
    AndroidPermissions,
    Base64ToGallery,

  ],
  declarations: [PaPage]
})
export class PaPageModule {}
