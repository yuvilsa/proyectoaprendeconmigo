import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UuPage } from './uu.page';

const routes: Routes = [
  {
    path: '',
    component: UuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UuPageRoutingModule {}
