import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LosNumerosPageRoutingModule } from './los-numeros-routing.module';

import { LosNumerosPage } from './los-numeros.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LosNumerosPageRoutingModule
  ],
  declarations: [LosNumerosPage]
})
export class LosNumerosPageModule {}
