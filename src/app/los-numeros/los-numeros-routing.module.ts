import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LosNumerosPage } from './los-numeros.page';

const routes: Routes = [
  {
    path: '',
    component: LosNumerosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LosNumerosPageRoutingModule {}
