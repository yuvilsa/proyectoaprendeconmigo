import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-burro',
  templateUrl: './burro.page.html',
  styleUrls: ['./burro.page.scss'],
})
export class BurroPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/ballena']);
  }
  Continuar(){
    this.router.navigate(['/gato']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/buu.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/rro.m4a');
    audio.load();
    audio.play();
  }
}
