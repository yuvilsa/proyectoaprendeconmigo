import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LasVocalesPage } from './las-vocales.page';

const routes: Routes = [
  {
    path: '',
    component: LasVocalesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LasVocalesPageRoutingModule {}
