import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LasVocalesPageRoutingModule } from './las-vocales-routing.module';

import { LasVocalesPage } from './las-vocales.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LasVocalesPageRoutingModule
  ],
  declarations: [LasVocalesPage]
})
export class LasVocalesPageModule {}
