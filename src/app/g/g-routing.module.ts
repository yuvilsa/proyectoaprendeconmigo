import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GPage } from './g.page';

const routes: Routes = [
  {
    path: '',
    component: GPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GPageRoutingModule {}
