import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { N4Page } from './n4.page';

const routes: Routes = [
  {
    path: '',
    component: N4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class N4PageRoutingModule {}
