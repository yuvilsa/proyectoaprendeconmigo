import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-caballo',
  templateUrl: './caballo.page.html',
  styleUrls: ['./caballo.page.scss'],
})
export class CaballoPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/conejo']);
  }
  Continuar(){
    this.router.navigate(['/deletrear']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/caa.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/baa.m4a');
    audio.load();
    audio.play();
  }
  Animales3(){
    let audio = new Audio('./assets/audio/Lloo.m4a');
    audio.load();
    audio.play();
  }
}
