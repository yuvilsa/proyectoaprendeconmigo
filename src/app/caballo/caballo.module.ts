import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CaballoPageRoutingModule } from './caballo-routing.module';

import { CaballoPage } from './caballo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CaballoPageRoutingModule
  ],
  declarations: [CaballoPage]
})
export class CaballoPageModule {}
