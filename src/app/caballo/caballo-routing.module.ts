import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CaballoPage } from './caballo.page';

const routes: Routes = [
  {
    path: '',
    component: CaballoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CaballoPageRoutingModule {}
