import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BallenaPage } from './ballena.page';

const routes: Routes = [
  {
    path: '',
    component: BallenaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BallenaPageRoutingModule {}
