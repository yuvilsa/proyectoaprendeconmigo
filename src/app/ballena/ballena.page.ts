import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-ballena',
  templateUrl: './ballena.page.html',
  styleUrls: ['./ballena.page.scss'],
})
export class BallenaPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/perro']);
  }
  Continuar(){
    this.router.navigate(['/burro']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/baa.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/lle.m4a');
    audio.load();
    audio.play();
  }
  Animales3(){
    let audio = new Audio('./assets/audio/naa.m4a');
    audio.load();
    audio.play();
  }
}
