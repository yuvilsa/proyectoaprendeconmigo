import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BallenaPageRoutingModule } from './ballena-routing.module';

import { BallenaPage } from './ballena.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BallenaPageRoutingModule
  ],
  declarations: [BallenaPage]
})
export class BallenaPageModule {}
