import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
/*import { SolRoutingModule } from './sol-routing.module';*/
import { SolPageRoutingModule } from './sol-routing.module';

import { SolPage } from './sol.page';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
/*import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';*/
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';

 
@NgModule({
  declarations: [SolPage],
  entryComponents: [],
  imports: [    CommonModule,
    FormsModule,
    IonicModule,
    SolPageRoutingModule,
    BrowserModule, 
    IonicModule.forRoot(), 
    /*SolRoutingModule*/],
  providers: [
   /* statusbar, 
   StatusBar, SplashScreen
    SplashScreen,*/
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Base64ToGallery
  ],
  bootstrap: [SolPage]
})
export class SolPageModule {}