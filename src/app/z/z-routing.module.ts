import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ZPage } from './z.page';

const routes: Routes = [
  {
    path: '',
    component: ZPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZPageRoutingModule {}
