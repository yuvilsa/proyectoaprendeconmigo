import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { APageRoutingModule } from './a-routing.module';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { APage } from './a.page';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    APageRoutingModule,
    
  ],
  providers: [
    AndroidPermissions,
    Base64ToGallery,
    NativeAudio,

  ],
  declarations: [APage]
})
export class APageModule {}
