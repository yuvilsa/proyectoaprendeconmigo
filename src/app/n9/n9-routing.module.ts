import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { N9Page } from './n9.page';

const routes: Routes = [
  {
    path: '',
    component: N9Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class N9PageRoutingModule {}
