import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { N6Page } from './n6.page';

const routes: Routes = [
  {
    path: '',
    component: N6Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class N6PageRoutingModule {}
