import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-deletrear',
  templateUrl: './deletrear.page.html',
  styleUrls: ['./deletrear.page.scss'],
})
export class DeletrearPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }
  Animales(){
    this.router.navigate(['/perro']);
  }
  Alimentos(){
    this.router.navigate(['/banano']);
  }
  Hogar(){
    this.router.navigate(['/silla']);
  }
  Herramientas(){
    this.router.navigate(['/martillo']); 
  }
  Regresar(){
    this.router.navigate(['/sub-menu-aprendizaje']);
  }
  Continuar(){
    this.router.navigate(['/perro']);
  }
}
