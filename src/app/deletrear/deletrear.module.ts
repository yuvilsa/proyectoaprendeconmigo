import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeletrearPageRoutingModule } from './deletrear-routing.module';

import { DeletrearPage } from './deletrear.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeletrearPageRoutingModule
  ],
  declarations: [DeletrearPage]
})
export class DeletrearPageModule {}
