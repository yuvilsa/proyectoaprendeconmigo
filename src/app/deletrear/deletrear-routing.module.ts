import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeletrearPage } from './deletrear.page';

const routes: Routes = [
  {
    path: '',
    component: DeletrearPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeletrearPageRoutingModule {}
