import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { N2PageRoutingModule } from './n2-routing.module';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { N2Page } from './n2.page';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    N2PageRoutingModule,
    
  ],
  providers: [
    AndroidPermissions,
    Base64ToGallery,

  ],
  declarations: [N2Page]
})
export class N2PageModule {}
