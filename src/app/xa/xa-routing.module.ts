import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { XaPage } from './xa.page';

const routes: Routes = [
  {
    path: '',
    component: XaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class XaPageRoutingModule {}
