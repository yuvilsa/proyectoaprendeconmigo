import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { XaPageRoutingModule } from './xa-routing.module';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { XaPage } from './xa.page';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    XaPageRoutingModule,
    
  ],
  providers: [
    AndroidPermissions,
    Base64ToGallery,

  ],
  declarations: [XaPage]
})
export class XaPageModule {}
