import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { RPageRoutingModule } from './r-routing.module';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { RPage } from './r.page';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RPageRoutingModule,
    
  ],
  providers: [
    AndroidPermissions,
    Base64ToGallery,

  ],
  declarations: [RPage]
})
export class RPageModule {}
