import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EdiPageRoutingModule } from './edi-routing.module';

import { EdiPage } from './edi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EdiPageRoutingModule
  ],
  declarations: [EdiPage]
})
export class EdiPageModule {}
