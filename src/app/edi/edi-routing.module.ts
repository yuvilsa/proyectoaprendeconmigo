import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EdiPage } from './edi.page';

const routes: Routes = [
  {
    path: '',
    component: EdiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EdiPageRoutingModule {}
