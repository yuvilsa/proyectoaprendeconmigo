import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { N0Page } from './n0.page';

const routes: Routes = [
  {
    path: '',
    component: N0Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class N0PageRoutingModule {}
