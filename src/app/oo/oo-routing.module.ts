import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OoPage } from './oo.page';

const routes: Routes = [
  {
    path: '',
    component: OoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OoPageRoutingModule {}
