import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AlertController } from '@ionic/angular';


import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
//import { IonicStorageModule } from '@ionic/storage';
import { IonicStorageModule } from '@ionic/storage-angular';

import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { HttpClientModule } from '@angular/common/http';

import { AgregarPage } from './agregar/agregar.page';
import { EditarPage } from './editar/editar.page';
import { Storage } from '@ionic/storage';



@NgModule({
  declarations: [AppComponent,
    AgregarPage,
    EditarPage
  ],
  entryComponents: [
    AgregarPage,
    EditarPage
  ],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, IonicStorageModule.forRoot()],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy,  useClass: IonicRouteStrategy },
    NativeAudio, 
    SQLitePorter,
    Storage, 
    SQLite],
    
  bootstrap: [AppComponent]
})
export class AppModule {}

