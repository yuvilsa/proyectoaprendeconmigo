import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QPage } from './q.page';

const routes: Routes = [
  {
    path: '',
    component: QPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QPageRoutingModule {}
