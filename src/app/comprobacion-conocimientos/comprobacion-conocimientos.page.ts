import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-comprobacion-conocimientos',
  templateUrl: './comprobacion-conocimientos.page.html',
  styleUrls: ['./comprobacion-conocimientos.page.scss'],
})
export class ComprobacionConocimientosPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  Regresar(){
    this.router.navigate(['/menu-principal']);
  }
  Instrucciones1(){
    let audio = new Audio('./assets/audio/b7.m4a');
    audio.load();
    audio.play();
  }
  Instrucciones2(){
    let audio = new Audio('./assets/audio/b8.m4a');
    audio.load();
    audio.play();
  }
  Instrucciones3(){
    let audio = new Audio('./assets/audio/b6.m4a');
    audio.load();
    audio.play();
  }
  Animales1(){
    let audio = new Audio('./assets/audio/b13.m4a');
    audio.load();
    audio.play();
    this.router.navigate(['/prueba1']);
  }

}
