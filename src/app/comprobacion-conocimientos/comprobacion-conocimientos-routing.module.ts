import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComprobacionConocimientosPage } from './comprobacion-conocimientos.page';

const routes: Routes = [
  {
    path: '',
    component: ComprobacionConocimientosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComprobacionConocimientosPageRoutingModule {}
