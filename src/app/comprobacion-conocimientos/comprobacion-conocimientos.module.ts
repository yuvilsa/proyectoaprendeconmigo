import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ComprobacionConocimientosPageRoutingModule } from './comprobacion-conocimientos-routing.module';

import { ComprobacionConocimientosPage } from './comprobacion-conocimientos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComprobacionConocimientosPageRoutingModule
  ],
  declarations: [ComprobacionConocimientosPage]
})
export class ComprobacionConocimientosPageModule {}
