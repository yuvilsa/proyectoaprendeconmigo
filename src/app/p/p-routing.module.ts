import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PPage } from './p.page';

const routes: Routes = [
  {
    path: '',
    component: PPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PPageRoutingModule {}
