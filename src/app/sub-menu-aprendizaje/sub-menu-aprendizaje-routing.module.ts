import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubMenuAprendizajePage } from './sub-menu-aprendizaje.page';

const routes: Routes = [
  {
    path: '',
    component: SubMenuAprendizajePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubMenuAprendizajePageRoutingModule {}
