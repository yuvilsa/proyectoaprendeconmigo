import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubMenuAprendizajePageRoutingModule } from './sub-menu-aprendizaje-routing.module';

import { SubMenuAprendizajePage } from './sub-menu-aprendizaje.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubMenuAprendizajePageRoutingModule
  ],
  declarations: [SubMenuAprendizajePage]
})
export class SubMenuAprendizajePageModule {}
