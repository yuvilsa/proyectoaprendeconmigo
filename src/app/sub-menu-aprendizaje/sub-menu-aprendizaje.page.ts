import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sub-menu-aprendizaje',
  templateUrl: './sub-menu-aprendizaje.page.html',
  styleUrls: ['./sub-menu-aprendizaje.page.scss'],
})
export class SubMenuAprendizajePage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  Vocales(){
    this.router.navigate(['/a']);
  }
  Abecedario(){
    this.router.navigate(['/aa']);
  }
  Lassilabas(){
    this.router.navigate(['/ba']);
  }
  Losnumeros(){
    this.router.navigate(['/n0']);
  }
  Deletrear(){
    this.router.navigate(['/deletrear']);
  }
  Completarpalabras(){
    this.router.navigate(['/completar-palabras']);
  }
  Regresar(){
    this.router.navigate(['/menu-principal']);
  }
  Instrucciones1(){
    let audio = new Audio('./assets/audio/b1.m4a');
    audio.load();
    audio.play();
  }
  Instrucciones2(){
    let audio = new Audio('./assets/audio/b2.m4a');
    audio.load();
    audio.play();
  }
  Instrucciones3(){
    let audio = new Audio('./assets/audio/b3.m4a');
    audio.load();
    audio.play();
  }
  Instrucciones4(){
    let audio = new Audio('./assets/audio/b4.m4a');
    audio.load();
    audio.play();
  }
  Instrucciones5(){
    let audio = new Audio('./assets/audio/b5.m4a');
    audio.load();
    audio.play();
  }

}
