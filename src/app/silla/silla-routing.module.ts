import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SillaPage } from './silla.page';

const routes: Routes = [
  {
    path: '',
    component: SillaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SillaPageRoutingModule {}
