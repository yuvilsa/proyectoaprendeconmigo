import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SillaPageRoutingModule } from './silla-routing.module';

import { SillaPage } from './silla.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SillaPageRoutingModule
  ],
  declarations: [SillaPage]
})
export class SillaPageModule {}
