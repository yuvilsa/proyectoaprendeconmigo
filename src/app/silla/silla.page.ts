import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-silla',
  templateUrl: './silla.page.html',
  styleUrls: ['./silla.page.scss'],
})
export class SillaPage implements OnInit {


  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  Regresar(){
    this.router.navigate(['/deletrear']);
  }
  Continuar(){
    this.router.navigate(['/sofa']);
  }
  Animales1(){
    let audio = new Audio('./assets/audio/sii.m4a');
    audio.load();
    audio.play();
  }
  Animales2(){
    let audio = new Audio('./assets/audio/llaa.m4a');
    audio.load();
    audio.play();
  }
}
