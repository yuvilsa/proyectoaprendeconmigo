import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ElAbecedarioPage } from './el-abecedario.page';

const routes: Routes = [
  {
    path: '',
    component: ElAbecedarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ElAbecedarioPageRoutingModule {}
