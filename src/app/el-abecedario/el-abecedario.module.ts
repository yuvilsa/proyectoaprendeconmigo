import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ElAbecedarioPageRoutingModule } from './el-abecedario-routing.module';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { ElAbecedarioPage } from './el-abecedario.page';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ElAbecedarioPageRoutingModule,
    
  ],
  providers: [
    AndroidPermissions,
    Base64ToGallery,

  ],
  declarations: [ElAbecedarioPage]
})
export class ElAbecedarioPageModule {}
