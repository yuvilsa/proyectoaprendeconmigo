import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AaPage } from './aa.page';

const routes: Routes = [
  {
    path: '',
    component: AaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AaPageRoutingModule {}
